package com.example.onboardingsample;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import static com.example.onboardingsample.MainActivity.KEY_ON_BOARDING_COMPLETED;

public class MainScreenFragment extends Fragment {
    private CheckBox onboardingStatusCheckBox;

    public MainScreenFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d(this.getClass().getName(), "onCreate");
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final SharedPreferences sharedPreferences = getActivity().getPreferences(Context.MODE_PRIVATE);
        boolean onBoardingCompleted = sharedPreferences.getBoolean(KEY_ON_BOARDING_COMPLETED, false);

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_main_screen, container, false);
        final Button nextButton = view.findViewById(R.id.next_btn);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NavController navController = Navigation.findNavController(nextButton);
                navController.navigate(R.id.action_mainScreenFragment_to_groupDatailFragment);
            }
        });

        final Button consentButton = view.findViewById(R.id.consent_btn);
        consentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NavController navController = Navigation.findNavController(consentButton);
                navController.navigate(R.id.action_mainScreenFragment_to_userConsentFragment);
            }
        });

        onboardingStatusCheckBox = view.findViewById(R.id.onboarding_status);
        onboardingStatusCheckBox.setChecked(onBoardingCompleted);

        onboardingStatusCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putBoolean(KEY_ON_BOARDING_COMPLETED, isChecked);
                editor.apply();
            }
        });

//        OnBoarding status: done
        return view;
    }

}
