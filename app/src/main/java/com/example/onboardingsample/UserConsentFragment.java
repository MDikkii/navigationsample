package com.example.onboardingsample;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import static com.example.onboardingsample.MainActivity.KEY_ON_BOARDING_COMPLETED;

public class UserConsentFragment extends Fragment {
    private boolean onBoardingCompleted;


    public UserConsentFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d(this.getClass().getName(), "onCreate");
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        SharedPreferences sharedPreferences = getActivity().getPreferences(Context.MODE_PRIVATE);
        onBoardingCompleted = sharedPreferences.getBoolean(KEY_ON_BOARDING_COMPLETED, false);
        View view = inflater.inflate(R.layout.fragment_user_consent, container, false);
        final Button nextButton = view.findViewById(R.id.next_btn);


        if(onBoardingCompleted){
            nextButton.setText("Done");
        } else {
            nextButton.setText("Next");
        }

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NavController navController = Navigation.findNavController(nextButton);
                if(onBoardingCompleted){
                    navController.navigate(R.id.action_userConsentFragment_to_mainScreenFragment);
                } else {
                    navController.navigate(R.id.action_userConsentFragment_to_profileFragment);
                }
            }
        });
        return view;
    }

}
