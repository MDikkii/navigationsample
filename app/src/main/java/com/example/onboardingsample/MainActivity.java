package com.example.onboardingsample;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;

public class MainActivity extends AppCompatActivity {

    public static final String KEY_ON_BOARDING_COMPLETED = "onBoardingCompleted";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar supportActionBar = findViewById(R.id.toolbar);
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupWithNavController(supportActionBar, navController);

        SharedPreferences sharedPreferences = getPreferences(Context.MODE_PRIVATE);
        boolean onBoardingCompleted = sharedPreferences.getBoolean(KEY_ON_BOARDING_COMPLETED, false);

        if(!onBoardingCompleted){
            navController.navigate(R.id.action_mainScreenFragment_to_helloFragment);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        return Navigation.findNavController(this, R.id.nav_host_fragment).navigateUp();
    }
}
